const uuid = require('uuid');
const path = require('path');
const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http, {cors: {
  origin: 'http://localhost:4200',
  methods: ["GET", "POST"]
}});

app.use(express.static(path.join(__dirname + '/dist/web-rtc')));

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/web-rtc/index.html'));
});

const rooms = new Map();

const user = (id, name) => {
  return {id, name}
}

const createRoom = (socketId, userName) => {
  const room = {id: uuid.v4(), host: user(socketId, userName), guest: {}};
  rooms.set(room.id, room);
  return room;
}

const joinRoom = (socketId, userName, roomId) => {
  const room = rooms.get(roomId);
  if (room.host.id !== socketId) {
    room.guest = user(socketId, userName);
  }
  return room;
}





io.on("connection", socket => {
  socket.on('create-room', ({userName}, callback) => {
    const room = createRoom(socket.id, userName);
    socket.join(room.id);
    callback({...room, isHost: socket.id === room.host.id});
  });

  socket.on('join-room', ({userName, roomId}, callback) => {
    try {
      const room = joinRoom(socket.id, userName, roomId);
      socket.join(room.id);
      callback({...room, isHost: socket.id === room.host.id});
    } catch {
      callback({error: true});
    }
  });

  socket.on('leave-room', ({roomId}) => {
    const room = rooms.get(roomId);
    if (room) {
      if (room.host.id === socket.id) {
        socket.to(roomId).emit('host-disconnect');
        rooms.delete(roomId);
      } else {
        socket.to(roomId).emit('guest-disconnect');
      }
    }
  });

  socket.on('kick-guest', ({roomId}) => {
    socket.to(roomId).emit('forced-disconnect');
  });

  socket.on('check-room', ({roomId}, callback) => {
    callback({roomExist: rooms.has(roomId)});
  });

  socket.on('offer', ({roomId, offer}) => {
    if (socket.rooms.has(roomId)) {
      socket.to(roomId).emit('offer', {offer, offerSocketId: socket.id});
    }
  });

  socket.on('answer', ({offerSocketId, answer}) => {
    socket.to(offerSocketId).emit('answer', {answer});
  });

  socket.on('candidate', ({candidate, roomId}) => {
    if (socket.rooms.has(roomId)) {
      socket.to(roomId).emit('candidate', {candidate});
    }
  });

  socket.on('disconnect', () => {
    const socketRooms = [...rooms.values()].filter(room => room.host.id === socket.id || room.guest.id === socket.id);
    socketRooms.forEach(room => {
      if (room.host.id === socket.id) {
        rooms.delete(room.id);
        socket.to(room.id).emit('host-disconnect', {disconnectId: socket.id});
        return;
      }
      socket.to(room.id).emit('guest-disconnect', {disconnectId: socket.id})
    });
  });
});

http.listen(process.env.PORT || 8080, () => {
  console.log('Listening on port 8080');
});