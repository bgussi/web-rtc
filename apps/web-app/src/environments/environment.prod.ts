export const environment = {
  production: true,
  wsEndpoint: 'https://yoda-rtc.herokuapp.com',
  iceConfig: {
    iceServers: [
      {
        urls: 'stun:stun.services.mozilla.com:3478'
      },
      {
        urls: 'stun:stun1.l.google.com:19302',
      },
    ],
  },
};
