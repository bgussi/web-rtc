import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModulesModule } from './material-modules/material-modules.module';
import { CreateUserDialogComponent } from './components/create-user-dialog/create-user-dialog.component';
import { SetupVideoModalComponent } from './components/setup-video-modal/setup-video-modal.component';
import { MicroSoundComponent } from './components/micro-sound/micro-sound.component';

const publicModules = [
  CommonModule,
  MaterialModulesModule,
  FormsModule,
  RouterModule,
  ReactiveFormsModule,
];

const publicComponents = [
  CreateUserDialogComponent,
  SetupVideoModalComponent,
  MicroSoundComponent,
];

@NgModule({
  imports: [
    ...publicModules,
  ],
  exports: [
    ...publicModules,
    ...publicComponents
  ],
  declarations: [
    ...publicComponents,
  ]
})
export class SharedModule { }
