import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';
import { MediaService } from '../../../core/services/media/media.service';

@Component({
  selector: 'app-setup-video-modal',
  templateUrl: './setup-video-modal.component.html',
  styleUrls: ['./setup-video-modal.component.scss']
})
export class SetupVideoModalComponent implements OnInit, OnDestroy {
  public rtcForm!: FormGroup;
  public stream$!: Observable<MediaStream | null>;
  private readonly destory$ = new Subject();
  constructor(
    private readonly mediaService: MediaService,
  ) { }

  public ngOnInit(): void {
    this.rtcForm = new FormGroup({
      video: new FormControl({value: true, disabled: true}),
      audio: new FormControl({value: true, disabled: true}),
    });

    this.mediaService.initLocalStream$().subscribe(() => this.rtcForm.enable());
    this.rtcForm.get('audio')?.valueChanges
      .pipe(takeUntil(this.destory$))
      .subscribe((play) => this.mediaService.setLocalAudio(play));
    this.rtcForm.get('video')?.valueChanges
      .pipe(takeUntil(this.destory$))
      .subscribe((play) => this.mediaService.setLocalVideo(play));

    this.stream$ = this.mediaService.localStream$;
  }

  public ngOnDestroy(): void {
      this.destory$.next(null);
      this.destory$.complete();
  }

}
