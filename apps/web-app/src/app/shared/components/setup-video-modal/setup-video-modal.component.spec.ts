import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupVideoModalComponent } from './setup-video-modal.component';

describe('SetupVideoModalComponent', () => {
  let component: SetupVideoModalComponent;
  let fixture: ComponentFixture<SetupVideoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupVideoModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupVideoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
