import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-create-user-dialog',
  templateUrl: './create-user-dialog.component.html',
  styleUrls: ['./create-user-dialog.component.scss']
})
export class CreateUserDialogComponent implements OnInit {
  public userForm!: FormGroup;
  constructor(
    private readonly userService: UserService,
    private readonly dialogRef: MatDialogRef<CreateUserDialogComponent>,
  ) { }

  public ngOnInit(): void {
    this.userForm = new FormGroup({
      name: new FormControl(null, Validators.required),
    });
  }

  public setUserName() {
    this.userService.setUser(this.userForm.value.name);
    this.dialogRef.close();
  }

}
