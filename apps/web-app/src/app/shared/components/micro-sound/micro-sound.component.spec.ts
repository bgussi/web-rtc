import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroSoundComponent } from './micro-sound.component';

describe('MicroSoundComponent', () => {
  let component: MicroSoundComponent;
  let fixture: ComponentFixture<MicroSoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MicroSoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroSoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
