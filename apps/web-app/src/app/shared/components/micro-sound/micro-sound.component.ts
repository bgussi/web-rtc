import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { BehaviorSubject, interval, Subject, timer } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-micro-sound',
  templateUrl: './micro-sound.component.html',
  styleUrls: ['./micro-sound.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MicroSoundComponent implements OnChanges, OnDestroy {
  @Input() public stream!: MediaStream;
  @Input() public isMicroOn!: boolean;
  public clipPath!: SafeStyle;
  private audioCtx = new AudioContext();
  private audioSource!: MediaStreamAudioSourceNode;
  private instant = 0.0;
  private script = this.audioCtx.createScriptProcessor(2048, 1, 1);
  private readonly destroy$ = new Subject();
  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly sanitaizer: DomSanitizer,
  ) {
    this.script.onaudioprocess = (event) => {
      const input = event.inputBuffer.getChannelData(0);
      let i;
      let sum = 0.0;
      let clipcount = 0;
      for (i = 0; i < input.length; ++i) {
        sum += input[i] * input[i];
        if (Math.abs(input[i]) > 0.99) {
          clipcount += 1;
        }
      }
      this.instant = Math.sqrt(sum / input.length);
    };

    interval(200)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.clipPath = this.sanitaizer.bypassSecurityTrustStyle(`inset(${(100 - this.instant * 1000).toFixed(2)}% 0 0 0)`);
        this.cdr.detectChanges()
      });
  }

  public ngOnChanges(): void {
    this.audioSource = this.audioCtx.createMediaStreamSource(this.stream);
    this.audioSource.connect(this.script);
    this.script.connect(this.audioCtx.destination);
  }

  public ngOnDestroy(): void {
      this.destroy$.next(null);
      this.destroy$.complete();
  }



}
