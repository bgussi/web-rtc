import { NgModule } from '@angular/core';

import { MeetingRoutingModule } from './meeting-routing.module';
import { MeetingComponent } from './meeting.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    MeetingComponent
  ],
  imports: [
    SharedModule,
    MeetingRoutingModule
  ]
})
export class MeetingModule { }
