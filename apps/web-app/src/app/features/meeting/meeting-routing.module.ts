import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeetingComponent } from './meeting.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '',
  },
  {
    path: ':meetingId',
    component: MeetingComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeetingRoutingModule { }
