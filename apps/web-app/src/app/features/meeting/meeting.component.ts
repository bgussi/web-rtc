import { Clipboard } from '@angular/cdk/clipboard';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { Room } from '../../core/interfaces/room';
import { MediaService } from '../../core/services/media/media.service';
import { RtcService } from '../../core/services/rtc/rtc.service';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent implements OnInit, AfterViewInit, OnDestroy {
  public room$!: Observable<Room>;
  public remoteStream$!: Observable<MediaStream | null>;
  public localStream$!: Observable<MediaStream>;
  public audioOn$!: Observable<boolean>;
  public videoOn$!: Observable<boolean>;
  constructor(
    private readonly rtcService: RtcService,
    private readonly mediaService: MediaService,
    private readonly cdr: ChangeDetectorRef,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly clipboard: Clipboard,
  ) { }

  public ngOnInit(): void {
    this.remoteStream$ = this.rtcService.remoteStream$;
    this.localStream$ = this.mediaService.localStream$;
    this.audioOn$ = this.mediaService.audioOn$;
    this.videoOn$ = this.mediaService.videoOn$;
    this.room$ = this.rtcService.room$;
    this.room$
      .pipe(first())
      .subscribe(room => {
        if (room.isHost) {
          this.rtcService.initHostRtc();
        } else {
          this.rtcService.initGuestRtc();
        }
      });
  }

  public toggleAudio(play: boolean) {
    this.mediaService.setLocalAudio(play);
  }

  public toggleVideo(play: boolean) {
    this.mediaService.setLocalVideo(play);
  }

  public ngAfterViewInit(): void {
    if (!this.rtcService.getRawRoom()) {
      this.rtcService.joinRoom$(this.activatedRoute.snapshot.params.meetingId).subscribe();
    }
    this.cdr.detectChanges();
  }

  public copyLink() {
    this.clipboard.copy(window.location.href);
  }

  public kickGuest() {
    this.rtcService.kickGuest();
  }

  public leave() {
    this.rtcService.leave();
    this.router.navigate(['/']);
  }

  public ngOnDestroy() {
    this.rtcService.leave();
  }

}
