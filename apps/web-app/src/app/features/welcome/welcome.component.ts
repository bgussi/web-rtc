import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RtcService } from '../../core/services/rtc/rtc.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  public joinForm!: FormGroup;
  constructor(
    private readonly rtcService: RtcService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.joinForm = new FormGroup({
      roomId: new FormControl(''),
    });
  }

  createMeeting() {
    this.rtcService.createRoom$().subscribe(room => {
      this.router.navigate([`/meeting/${room.id}`]);
    });
  }

  joinMeeting(roomId: string) {
    this.rtcService.joinRoom$(roomId)
    .subscribe(room => {
      this.router.navigate([`/meeting/${room.id}`]);
    });
  }

}
