import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WrapperGuardGuard } from './core/guards/wrapper-guard.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/welcome/welcome.module').then(m => m.WelcomeModule),
    pathMatch: 'full',
  },
  {
    path: 'meeting',
    loadChildren: () => import('./features/meeting/meeting.module').then(m => m.MeetingModule),
    canActivate: [WrapperGuardGuard],
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
