import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, tap, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MediaService {
  
  private readonly _localStream$ = new BehaviorSubject<MediaStream | null>(null);
  private readonly _audioOn$ = new BehaviorSubject<boolean>(false);
  private readonly _videoOn$ = new BehaviorSubject<boolean>(false);
  public get localStream$(): Observable<MediaStream> {
    return this._localStream$.asObservable().pipe(filter(stream => !!stream)) as Observable<MediaStream>;
  }
  public get audioOn$() {
    return this._audioOn$.asObservable();
  }
  public get videoOn$() {
    return this._videoOn$.asObservable();
  }

  public getLocalStream() {
    return this._localStream$.value;
  }

  public initLocalStream$(audio = true, video = true) {
    this.stopLocalStream();
    return new Observable(source => {
      navigator.mediaDevices.getUserMedia({
        audio,
        video
      }).then((stream) => {
        source.next(stream);
        source.complete();
      }).catch(() => {
        source.error('Without permission given, you won\'t be able to communicate');
      })
    }).pipe(
      tap(stream => this._localStream$.next(stream as MediaStream))
    );
  }

  public stopLocalStream() {
    const localStream = this._localStream$.value;
    if (localStream instanceof MediaStream) {
      localStream.getTracks().forEach(track => track.stop());
    }
    this._localStream$.next(null);
  }

  public setLocalAudio(play: boolean) {
    this.localStream$.pipe(first()).subscribe(localStream => {
      localStream.getAudioTracks()[0].enabled = play;
      this._audioOn$.next(localStream.getAudioTracks()[0].enabled);
    });
  }

  public setLocalVideo(play: boolean) {
    this.localStream$.pipe(first()).subscribe(localStream => {
      localStream.getVideoTracks()[0].enabled = play;
      this._videoOn$.next(localStream.getVideoTracks()[0].enabled);
    });
  }
}
