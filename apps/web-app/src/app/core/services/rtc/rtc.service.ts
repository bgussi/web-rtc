import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'apps/web-app/src/environments/environment';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { filter, first, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { Room } from '../../interfaces/room';
import { MediaService } from '../media/media.service';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class RtcService {
  private readonly _remoteStream$ = new BehaviorSubject<MediaStream | null>(null);
  private readonly _room$ = new BehaviorSubject<Room | null>(null);
  private readonly destroySubs$ = new Subject();
  private peerConnection!: RTCPeerConnection;
  constructor(
    private readonly socket: Socket,
    private readonly userService: UserService,
    private readonly mediaService: MediaService,
    private readonly router: Router,
  ) {}

  public get remoteStream$() {
    return this._remoteStream$.asObservable();
  }
  public get room$(): Observable<Room> {
    return this._room$.asObservable().pipe(filter(room => !!room)) as Observable<Room>;
  }

  public checkRoom$(roomId: string): Observable<boolean> {
    return new Observable(source => {
      this.socket.emit('check-room', {roomId}, ({roomExist}: {roomExist: boolean}) => {
        source.next(roomExist);
        source.complete();
      });
    });
  }

  public createRoom$() {
    return this.userService.user$.pipe(
      first(),
      tap(user => this.socket.emit('create-room', {userName: user}, (room: Room) => this._room$.next(room))),
      switchMap(() => this.room$)
    );
  }

  public joinRoom$(roomId: string) {
    return this.userService.user$.pipe(
      first(),
      map(user => this.socket.emit('join-room', {userName: user, roomId}, (room: Room) => {
        if(room.error) {
          throw new Error('Room does not exist');
        }
        this._room$.next(room);
      })),
      switchMap(() => this.room$)
    );
  }

  public getRawRoom() {
    return this._room$.value;
  }

  public initHostRtc() {
    this.handleSDPs();
    this.preparePeerConnection();
    this.handleDisconnect();
  }
  public initGuestRtc() {
    this.handleSDPs();
    this.preparePeerConnection();
    this.sendOffer();
    this.handleDisconnect();
  }
  public kickGuest() {
    this.socket.emit('kick-guest', {roomId: this._room$.value?.id});
    this._remoteStream$.next(null);
  }

  public leave() {
    this.socket.emit('leave-room', {roomId: this._room$.value?.id});
    this.disconnect();
  }
  public disconnect() {
    this.destroySubs$.next(null);
    this.destroySubs$.complete();
    this._room$.next(null);
    this._remoteStream$.next(null);
    this.peerConnection.close();
    this.mediaService.stopLocalStream();
  }
  private handleDisconnect() {
    this.socket.fromEvent('guest-disconnect')
      .pipe(takeUntil(this.destroySubs$))
      .subscribe(() => {
        this._remoteStream$.next(null);
      });
    this.socket.fromEvent('host-disconnect')
      .pipe(takeUntil(this.destroySubs$))
      .subscribe(() => {
        this._remoteStream$.next(null);
        this.disconnect();
        this.router.navigate(['/']);
      });
    this.socket.fromEvent('forced-disconnect')
      .pipe(takeUntil(this.destroySubs$))
      .subscribe(() => {
        this._remoteStream$.next(null);
        this.disconnect();
        this.router.navigate(['/']);
      });
  }
  private preparePeerConnection() {
    this.peerConnection = new RTCPeerConnection(environment.iceConfig);
    this.peerConnection.onicecandidate = (event: RTCPeerConnectionIceEvent) => {
      if(event.candidate) {
       this.sendIce(event.candidate);
      }
    };
    this.peerConnection.ontrack = (ev: RTCTrackEvent) => {
      this._remoteStream$.next(ev.streams[0]);
    }
    
    const stream = this.mediaService.getLocalStream()
    stream?.getTracks().forEach(track => {
      this.peerConnection.addTrack(track, stream)
    });
  }

  
  private sendIce(candidate: RTCIceCandidate) {
    this.socket.emit('candidate', {candidate, roomId: this._room$.value?.id});
  }
  private sendOffer() {
    this.peerConnection.createOffer()
      .then((offer) => {
        this.socket.emit('offer', {offer, roomId: this._room$.value?.id});
        return offer;
      }).then((offer) => this.peerConnection.setLocalDescription(offer))
  }
  private handleOffer({offer, offerSocketId}: {offer: RTCSessionDescriptionInit, offerSocketId: string}) {
    this.peerConnection.setRemoteDescription(offer)
      .then(() => this.peerConnection.createAnswer())
      .then(answer => {
        this.socket.emit('answer', {answer, offerSocketId});
        return answer;
      }).then((answer) => this.peerConnection.setLocalDescription(answer));
  }
  private handleAnswer(answer: RTCSessionDescriptionInit) {
    this.peerConnection.setRemoteDescription(new RTCSessionDescription(answer));
  }
  private handleCandidate(candidate: RTCIceCandidateInit) {
    this.peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
  }
  private handleSDPs() {
    this.socket.fromEvent<{offer: RTCSessionDescriptionInit, offerSocketId: string}>('offer')
      .pipe(takeUntil(this.destroySubs$))
      .subscribe(response => this.handleOffer(response));
    this.socket.fromEvent('answer')
      .pipe(takeUntil(this.destroySubs$))
      .subscribe((response: any) => this.handleAnswer(response.answer));
    this.socket.fromEvent('candidate')
      .pipe(takeUntil(this.destroySubs$))
      .subscribe((response: any) => this.handleCandidate(response.candidate))
  }
}
