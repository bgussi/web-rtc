import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly _user$ = new ReplaySubject<string>(1);
  private readonly LS_KEY = 'user';
  constructor() {
    this._user$.next(localStorage.getItem(this.LS_KEY) || '');
  }
  public get user$() {
    return this._user$.asObservable();
  }
  public setUser(user: string) {
    localStorage.setItem(this.LS_KEY, user);
    this._user$.next(user);
  }
}
