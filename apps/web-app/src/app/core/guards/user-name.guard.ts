import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CanActivate } from '@angular/router';
import { merge, Observable, partition } from 'rxjs';
import { map, mapTo, switchMap } from 'rxjs/operators';
import { UserService } from '../services/user.service';
import { CreateUserDialogComponent } from '../../shared/components/create-user-dialog/create-user-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class UserNameGuard implements CanActivate {
  constructor(
    private readonly dialog: MatDialog,
    private readonly userService: UserService,
  ) {}
  canActivate(): Observable<boolean> {
    const [isUserValid$, isUserNotValid$] = partition(this.userService.user$.pipe(map(Boolean)), isUserValid => !!isUserValid);
    const userValidation$ = isUserNotValid$.pipe(
      switchMap(() => this.dialog.open(CreateUserDialogComponent, {disableClose: true}).afterClosed()),
      mapTo(true),
    );
    return merge(isUserValid$, userValidation$);
  }
  
}
