import { TestBed } from '@angular/core/testing';

import { UserNameGuard } from './user-name.guard';

describe('UserNameGuard', () => {
  let guard: UserNameGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UserNameGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
