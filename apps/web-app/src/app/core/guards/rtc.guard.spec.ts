import { TestBed } from '@angular/core/testing';

import { RtcGuard } from './rtc.guard';

describe('RtcGuard', () => {
  let guard: RtcGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RtcGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
