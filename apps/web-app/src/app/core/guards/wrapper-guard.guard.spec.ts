import { TestBed } from '@angular/core/testing';

import { WrapperGuardGuard } from './wrapper-guard.guard';

describe('WrapperGuardGuard', () => {
  let guard: WrapperGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(WrapperGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
