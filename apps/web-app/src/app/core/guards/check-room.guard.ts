import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RtcService } from '../services/rtc/rtc.service';

@Injectable({
  providedIn: 'root'
})
export class CheckRoomGuard implements CanActivate {
  constructor(
    private readonly rtcService: RtcService,
    private readonly router: Router,
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const strings = state.url.split('/');
    const meetingId = strings[strings.length - 1];
    return this.rtcService.checkRoom$(meetingId).pipe(
      tap(roomExist => {
        if (!roomExist) {
          this.router.navigate(['/']);
        }
      })
    );
  }
  
}
