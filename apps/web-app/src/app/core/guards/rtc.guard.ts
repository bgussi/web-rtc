import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CanActivate, Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { SetupVideoModalComponent } from '../../shared/components/setup-video-modal/setup-video-modal.component';
import { MediaService } from '../services/media/media.service';

@Injectable({
  providedIn: 'root'
})
export class RtcGuard implements CanActivate {
  constructor(
    private readonly dialog: MatDialog,
    private readonly mediaService: MediaService,
    private readonly router: Router,
  ) {}
  public canActivate() {
    return this.dialog.open(SetupVideoModalComponent, {disableClose: true}).afterClosed()
      .pipe(
        map(result => !!result),
        tap(result => {
          if (!result) {
            this.mediaService.stopLocalStream();
            this.router.navigate(['/']);
          }
        })
      )
  }
  
}
