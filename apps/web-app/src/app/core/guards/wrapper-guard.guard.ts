import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CheckRoomGuard } from './check-room.guard';
import { RtcGuard } from './rtc.guard';
import { UserNameGuard } from './user-name.guard';

@Injectable({
  providedIn: 'root'
})
export class WrapperGuardGuard implements CanActivate {
  constructor(
    private readonly checkRoomGuard: CheckRoomGuard,
    private readonly userNameGuard: UserNameGuard,
    private readonly rtcGuard: RtcGuard,
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.checkRoomGuard.canActivate(route, state)
      .pipe(
        switchMap((roomExist) => {
          if (roomExist) {
            return this.userNameGuard.canActivate()
              .pipe(
                switchMap((isUserValid) => {
                  if(isUserValid) {
                    return this.rtcGuard.canActivate();
                  }
                  return of(false);
                })
              );
          }
          return of(false);
        })
      );
  }
  
}
