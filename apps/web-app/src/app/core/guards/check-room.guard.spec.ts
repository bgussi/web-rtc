import { TestBed } from '@angular/core/testing';

import { CheckRoomGuard } from './check-room.guard';

describe('CheckRoomGuard', () => {
  let guard: CheckRoomGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CheckRoomGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
