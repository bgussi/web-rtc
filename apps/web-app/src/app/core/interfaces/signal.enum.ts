export enum Signal {
  OFFER = 'offer',
  ANSWER = 'answer',
  CANDIDATE = 'candidate',
}