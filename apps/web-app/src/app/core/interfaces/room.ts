interface RoomUser {
  id: string;
  name: string;
}

export interface Room {
  id: string;
  host: RoomUser;
  guests: RoomUser[];
  isHost: boolean;
  error: boolean;
}